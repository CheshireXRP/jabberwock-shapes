
var img = document.createElement("img");
img.src = "none";
var append = document.getElementById("overlay-header").appendChild(img);
var pixels;
var catcanvas = document.createElement("canvas");
console.log("There you are :)");
console.log("Cheshire Cat will fetch!");
gl.ondraw = function() {
	if (gl.orbitalView) {
		var e = appState.backgroundColor;
		if (gl.clearColor(e[0], e[1], e[2], 0),
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT),
		gl.orbitalView.apply(!0),
		gl.disable(gl.CULL_FACE),
		gl.enable(gl.BLEND),
		gl.pushMatrix(),
		gl.scale(superShape.scale, superShape.scale, superShape.scale),
		!isShapeChanging && appData.silhouetteWidth > .01 && (gl.depthMask(!1),
		gl.lineWidth(appData.silhouetteWidth * pd.devicePixelRatio),
		silhouetteShader.uniform("color", appState.silhouetteColor).drawOutline(superShape.getMesh()),
		appData.modelOpacity < .99 && silhouetteShader.uniform("color", e).drawSurface(superShape.getMesh()),
		gl.depthMask(!0)),
		gl.orbitalView.eyePos.z > shadowHeight) {
			gl.pushMatrix(),
			gl.depthMask(!1),
			gl.multMatrix(shadowTransform.toMatrix()),
			gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
			var a = Math.sin(pd.constrainTo(2 * (gl.orbitalView.eyePos.z - shadowHeight), 0, 1) * pd.HALF_PI);
			shadowShader.uniform("opacity", a).drawSurface(shadowMesh),
			gl.depthMask(!0),
			gl.popMatrix()
		}
		if (appData.modelOpacity < .99 && appData.modelOpacity > .001 && appState.outlineColor[3] > .01 && appData.outlineWidth > .01 && (gl.depthMask(!1),
		gl.lineWidth(appData.outlineWidth),
		isTransitioning ? interpolatingOutlineShader.uniforms({
			transition: transitionFactor,
			color: appState.outlineColor
		}).draw(superShape.getMesh(), gl.LINES) : outlineShader.drawOutline(superShape.getMesh()),
		gl.depthMask(!0)),
		appData.modelOpacity > .001) {
			if (appData.modelOpacity < .99 && gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA),
			isTransitioning) {
				var t = superShape.shapeColor()
				  , r = superShape.getLastShapeColor()
				  , i = t == COLOR_TEXTURE || r == COLOR_TEXTURE
				  , s = 1;
				i && (t == COLOR_TEXTURE && r != COLOR_TEXTURE ? s = transitionFactor : t != COLOR_TEXTURE && r == COLOR_TEXTURE && (s = 1 - transitionFactor),
				textureUVGrid.bind(1)),
				interpolatingSurfaceShader.uniforms({
					opacity: appData.modelOpacity,
					shininess: appData.modelShininess,
					transition: transitionFactor,
					ambientColor: appState.ambientColorArray,
					lightPosition: appState.lightPosition,
					textureMix: s,
					useTexture: i,
					texture: 1
				}).draw(superShape.getMesh()),
				i && textureUVGrid.unbind(1)
			} else
				surfaceShader.uniform("opacity", appData.modelOpacity).uniform("shininess", appData.modelShininess).uniform("ambientColor", appState.ambientColorArray).uniform("lightPosition", appState.lightPosition).drawSurface(superShape.getMesh());
			appData.modelOpacity < .99 && gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA)
		}
		appState.outlineColor[3] > .01 && appData.outlineWidth > .01 && (gl.lineWidth(appData.outlineWidth),
		isTransitioning ? interpolatingOutlineShader.uniforms({
			transition: transitionFactor,
			color: appState.outlineColor
		}).draw(superShape.getMesh(), gl.LINES) : outlineShader.drawOutline(superShape.getMesh())),
		gl.popMatrix(),
		gl.enable(gl.CULL_FACE),
		gl.disable(gl.BLEND)

		// Let me see! Let me see!
		pixels = new Uint8Array(gl.drawingBufferWidth * gl.drawingBufferHeight * 4);
		gl.readPixels(
			0,
			0,
			gl.drawingBufferWidth,
			gl.drawingBufferHeight,
			gl.RGBA,
			gl.UNSIGNED_BYTE,
			pixels);

		// The itty bitty spider
		img.height = 20;
		img.width = 20;

		//document.getElementById('canvas').style.visibility = "hidden";

		catcanvas.width = gl.drawingBufferWidth;
		catcanvas.height = gl.drawingBufferHeight;
		var flatcontext = catcanvas.getContext('2d');
		var imageData = flatcontext.createImageData(gl.drawingBufferWidth, gl.drawingBufferHeight);
		imageData.data.set(pixels);
		flatcontext.putImageData(imageData, 0, 0);

		// Shove them pixels into our image
		img.src = catcanvas.toDataURL();

	}
};
